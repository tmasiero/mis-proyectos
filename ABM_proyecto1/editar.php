<html>
    <head>
        <meta charset="UTF-8">
        <title>Empleados</title>
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Margarine" rel="stylesheet"> 
        <script src="js/jquery_v3.3.1.js"></script>
        <script src="js/ajax.js"></script>
    </head>
    <body>
        <div class="bg-amarillo contenedor sombra">
            <form class="inscribir widget" id="inscribir" action="#" style="display:none;">
                <div id="editando">
                    <legend>Editar empleado <span>Todos los campos son obligatorios</span></legend>
                </div>
                    <div class="campos">
                        <input type="hidden" id="id" name="id"/>
                        <div class="campo">
                            <label for="nombre">Nombre:</label>
                            <input type="text" name="nombre" id="nombre" placeholder="NOMBRE" required>
                        </div>
                        <div class="campo">
                            <label for="nombre">Email:</label>
                            <input type="text" name="email" id="email" placeholder="E-MAIL" required>
                        </div>
                        <div class="campo">
                            <label for="nombre">Telefono:</label>
                            <input type="tel" name="telefono" id="telefono" placeholder="TELEFONO" required>
                        </div>
                        <div class="campo">
                            <label for="nombre">Comentario:</label>
                            <input type="text" name="comentario" id="comentario" placeholder="Escriba comentario corto" required>
                        </div>
                    </div>
                <div class="campo enviar">
                        <input type="hidden" id="accion" value="editar">
                        <input type="submit" id="envio" value="Añadir">
                        <a href="index.php" class="btn volver">Volver</a>
                </div>
            </form>
        </div> 
    </body>
</html>
