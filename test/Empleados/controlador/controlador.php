<?php
session_start();
$tabla_empleados = new Consultas();
if (isset($_POST['cadena'])) {
    switch ($_POST['cadena']) {
        case 'Agregado:':
            echo json_encode($tabla_empleados->getRegister());
            break;
        case 'tabla_editar':
            echo json_encode($tabla_empleados->getDatosSelect());
            break;
        case 'tabla_personas':
            echo json_encode($tabla_empleados->getDatosAll());
            break;
        case 'archivo':
            echo json_encode($tabla_empleados->guardarArchivo());
            break;
        case 'update':
            echo json_encode($tabla_empleados->updateDatos());
            break;
        case 'subir':
            echo json_encode($tabla_empleados->subir_archivo());
            break;
    }
}

class Consultas {
    //coneccion a base de datos
    protected $con;
    private $dbuser = 'root';
    private $dbhost = 'localhost';
    private $pass = '';
    private $nbd = 'prueba2';

    public function __construct() {
        $this->con = $con = new mysqli($this->dbhost, $this->dbuser, $this->pass, $this->nbd);
        if (!isset($this->con)) {
            echo "ERR al crear coneccion";
        }
    }
    
    
    function getDatosAll() {
                
        $consultaPersonas = $this->con->query("SELECT * FROM people");

        $array = array();

        if ($consultaPersonas->data_seek(0)) {
            while ($row = $consultaPersonas->fetch_array(MYSQLI_ASSOC)) {
                $array[] = $row;
            }
        }
        return $array;
    }

    function getRegister() {
        $corto = implode("".PHP_EOL,$_POST);
        $nombre = $_POST['nombre'];
        $email = $_POST['email'];
        $telefono = $_POST['telefono'];
        $comentario= $_POST['comentario'];
        $nombre_archivo = $_POST['nombre_archivo'];
        $ext = '.txt';
        
        $fh = fopen($nombre_archivo.$ext, 'w') or die("Se produjo un error al crear el archivo");

        $link = "INSERT INTO people(nombre,email,telefono,comentario,nombre_archivo)VALUES('$nombre','$email','$telefono','$comentario','$nombre_archivo')";
        $consulta = $this->con->query($link);

        if ($consulta) {
            echo "200 ok";                
            $texto = <<<_END
$corto
_END;
            fwrite($fh, $texto) or die("No se pudo escribir en el archivo");
            fclose($fh);

        } else {
            mysqli_error($consulta);
            echo 'ERR al insertar datos';
        }
        
        mysqli_close($consulta);
        return array();
    }
    
    function getDatosSelect(){
        $id = $_POST['id'];

        $link = "SELECT * FROM people WHERE id = '$id'";
        $consulta = $this->con->query($link);
        $array = array();
        
        if ($consulta->data_seek(0)) {
            while ($row = $consulta->fetch_array(MYSQLI_ASSOC)) {
                $array[] = $row;

            }
        }
        return $array;
    }
    
    function  updateDatos(){
        $corto = implode("".PHP_EOL,$_POST);
        
        $id = $_POST['id'];
        $nombre = $_POST['nombre'];
        $email = $_POST['email'];
        $telefono = $_POST['telefono'];
        $comentario= $_POST['comentario'];
        $nombre_archivo = $_POST['nombre_archivo'];
        
        $ext = '.txt';
        
        $fh = fopen($nombre_archivo.$ext, 'w') or die("Se produjo un error al crear el archivo");
        
        $link2= "UPDATE `people` SET `nombre`= '$nombre',`email`= '$email',`telefono`='$telefono',`comentario`='$comentario', `nombre_archivo`='$nombre_archivo' WHERE id = '$id'";
        $consulta2 = $this->con->query($link2);
       if ($consulta2) {
            echo "200 ok";                
            $texto = <<<_END
$corto
_END;
            fwrite($fh, $texto) or die("No se pudo escribir en el archivo");
            fclose($fh);

        } else {
            mysqli_error($consulta2);
            echo 'ERR al insertar datos';
        }
        return array();
    }
    
    function guardarArchivo(){
        
        $link3 = "SELECT * FROM people";
        $consulta = $this->con->query($link3);
        
        $fh = fopen("prueba.txt", 'w') or die("Se produjo un error al crear el archivo");
        $array = array();
         
        if($consulta){
            if ($consulta->data_seek(0)) {
            while ($row = $consulta->fetch_array(MYSQLI_ASSOC)) {
                $array[] = $row;
            }
            
            $out = implode(PHP_EOL."------".PHP_EOL."Persona:" ,array_map(function($a) {
                    return implode("".PHP_EOL, $a);
                }, $array));
            print_r($out);
            $texto = <<<_END
$out
_END;
            fwrite($fh, $texto) or die("No se pudo escribir en el archivo");
            fclose($fh);
            
            header("index.php");
            }
        }else {
            echo 'ERR al guardar los datos'; 
        }
        
    }
   
//    function subir_archivo(){
//        $route = "subidas/";
//
//        $route = $route . basename($_FILES['file']['name']);
//        move_uploaded_file($_FILES['file']['tmp_name'], $route);
//    }
}

?>
